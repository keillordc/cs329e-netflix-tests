#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from Netflix import predict_rating
from Netflix import create_cache
from Netflix import create_custom_cache
from Netflix import calculate_RMSE
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----
    
    ACTUAL_CUSTOMER_RATING = create_cache("cache-actualCustomerRating.pickle")


    def test_eval_1(self):
        r = StringIO("1:\n30878\n2647871\n1283744\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(w.getvalue(), "1:\n3.946815\n3.601315\n3.764305\n0.49\n")
    
    
    def test_eval_2(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual( w.getvalue(), "10040:\n3.210805\n3.244315\n3.956815\n0.85\n")

    def test_eval_3(self):
        r = StringIO("2134:\n2345579\n2170922\n1373550\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(w.getvalue(), "2134:\n4.611315\n3.901315\n3.888315\n0.67\n")


    def test_prediction_1(self):
        c_id = 975179
        m_id = 10015
        prediction = predict_rating(c_id, m_id)
        self.assertEqual(prediction, 3.984315)
    
    def test_prediction_2(self):
        c_id = 30878
        m_id = 1
        prediction = predict_rating(c_id, m_id)
        self.assertEqual(prediction, 3.946815)

    def test_prediction_3(self):
        c_id = 851668
        m_id = 581
        prediction = predict_rating(c_id, m_id)
        self.assertEqual(prediction, 4.310315)        

    def test_prediction_valid_ratings(self):
        c_id = 268014
        m_id = 10022
        pred = predict_rating(c_id, m_id)
        self.assertTrue(pred>=1 and pred<=5)

    def test_prediction_valid_cust(self):
        c_id = 0
        m_id = 102
        self.assertRaises(AssertionError, predict_rating, c_id, m_id)

    def test_prediction_valid_movie(self):
        c_id = 100
        m_id = 100000
        self.assertRaises(AssertionError, predict_rating, c_id, m_id)

    def test_calculate_RMSE_valid_value(self):
        predictions = [5, 5, 3, 4]
        actual = [1, 5, 2, 4, 2]
        self.assertRaises(AssertionError, calculate_RMSE, predictions, actual) 
        
    def test_create_custom_cache(self):
        a1 = [1, 44, 0]
        a2 = [0]
        self.assertRaises(AssertionError, create_custom_cache, a1, a2)


    
# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
