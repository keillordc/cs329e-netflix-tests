#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.29\n3.46\n3.83\n0.94\n")

    def test_eval_2(self):
        r = StringIO("12501:\n61310\n545137\n")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(
            w.getvalue(),"12501:\n4.0\n4.29\n0.86\n")

    def test_eval_3(self):
        r = StringIO("1865:\n2117160\n1549490\n28277\n70101\n")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(
            w.getvalue(),"1865:\n3.66\n3.51\n3.69\n3.61\n0.79\n")
	
# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
